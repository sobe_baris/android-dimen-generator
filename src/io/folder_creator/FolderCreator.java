package io.folder_creator;

import java.io.File;

public final class FolderCreator {

	private FolderCreator() {
	};

	static boolean createFolder(String folderName) {
		return (new File(folderName)).mkdirs();
	}

	static boolean createFolder(String destinationPath, String folderName) {
		return (new File(destinationPath + folderName)).mkdirs();
	}

	static File getFolder(String path, String folderName) {
		return new File(path + folderName);
	}

	public static class Builder {
		private String path;
		private String folderName;
		private boolean isDeleteExistingFolderIfExits;

		public Builder setDestinationPath(String path) {
			this.path = path;
			return this;
		}

		public Builder setFolderName(String folderName) {
			this.folderName = folderName;
			return this;
		}

		public Builder setDeleteExistingFolderIfExits(boolean isDeleteExistingFolderIfExits) {
			this.isDeleteExistingFolderIfExits = isDeleteExistingFolderIfExits;
			return this;
		}

		public boolean build() {
			if (folderName == null || folderName.isEmpty()) {
				throw new NullPointerException("Folder Name should not be empty or null");
			}

			if (path == null) {
				return createFolder(folderName);
			} else {
				if (path.isEmpty()) {
					System.out.println("FolderCreator: Path is empty.");
					return createFolder(folderName);
				} else {
					if (isDeleteExistingFolderIfExits) {
						File folder = getFolder(path, folderName);
						if (folder.exists()) {
							folder.delete();
						}
						return createFolder(path, folderName);
					} else {
						return createFolder(path, folderName);
					}
				}
			}
		}
	}
}
