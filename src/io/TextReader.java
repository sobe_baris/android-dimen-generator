package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.Constants;
import pojo.DimensionModel;

public class TextReader {
	private File file;
	private Pattern p = Pattern.compile("<dimen name=\"([^\"]*)\">([^\"]*)</dimen>");

	public TextReader(String fileName) throws IOException {
		this.file = new File(new File(".").getCanonicalPath() + File.separator + fileName);
	}

	public List<DimensionModel> getDimensionList() {
		try {
			List<DimensionModel> dimensionList = new ArrayList<>();
			FileInputStream fileInputStream = new FileInputStream(this.file);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

			String line = null;
			Matcher matcher = null;
			while ((line = bufferedReader.readLine()) != null) {
				matcher = p.matcher(line);
				if (matcher.find()) {
					dimensionList.add(new DimensionModel(matcher.group(1), matcher.group(2)));
				}
			}

			bufferedReader.close();
			return dimensionList;
		} catch (Exception e) {
			System.err.println("Input file can not read!");
			System.err.println("Supported input file extensions ->\n.txt, .xml, etc..");
			System.err.println(Constants.EXAMPLE_USAGE);
			System.exit(-1);
		}
		return null;
	}

	/*
	 * private String parseResourcesKey(String line) { // <dimen
	 * name="view_margin_xsmall">3dp</dimen>
	 * 
	 * String parsedKey = null; String parsedValue = null; Matcher matcher =
	 * p.matcher(line); if (matcher.find()) { parsedKey = matcher.group(1);
	 * parsedValue = matcher.group(2); } return parsedKey; }
	 */
}
