package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.UnexpectedException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import pojo.DimensionModel;

public class TextWriter {
	/*
	 * private File resourcesFile;
	 * 
	 * public TextWriter(String dirName, String fileName) throws IOException {
	 * File dir = new File("output/" + dirName); dir.mkdirs(); resourcesFile =
	 * new File(dir, fileName); if (resourcesFile.exists()) {
	 * resourcesFile.delete(); } resourcesFile.createNewFile(); }
	 * 
	 * public void write(List<String> resourceList) { try { BufferedWriter
	 * writer = new BufferedWriter(new FileWriter(resourcesFile)); for (String
	 * resource : resourceList) { writer.write(resource); writer.newLine(); }
	 * writer.close(); } catch (Exception e) { e.printStackTrace(); } }
	 */

	public static void write(String dirName, String fileName, List<String> resourceList) {
		File dir = new File("output/" + dirName);
		dir.mkdirs();
		File resourcesFile = new File(dir, fileName);
		if (resourcesFile.exists()) {
			resourcesFile.delete();
		}
		try {
			resourcesFile.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(resourcesFile));
			for (String resource : resourceList) {
				writer.write(resource);
				writer.newLine();
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Pattern p = Pattern.compile("<dimen name=\"([^\"]*)\">([^\"]*)</dimen>");

	public static void write(File originalFile, File targetFile, List<DimensionModel> convertedList)
			throws UnexpectedException {
		if (targetFile.exists()) {
			targetFile.delete();
		}
		if (!targetFile.exists()) {
			try {
				boolean isSuccessfullyCreated = targetFile.createNewFile();
				if (!isSuccessfullyCreated) {
					throw new UnexpectedException(
							String.format("File: %s can not created", targetFile.getPath().toString()));
				}
			} catch (IOException exception) {
				exception.printStackTrace();
			}
		}

		FileInputStream fileInputStream = null;
		BufferedReader bufferedReader = null;
		BufferedWriter writer = null;

		try {
			fileInputStream = new FileInputStream(originalFile);
			bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
			writer = new BufferedWriter(new FileWriter(targetFile));
			String line = null;
			Matcher matcher = null;
			Stream<String> lineStream = bufferedReader.lines();
			Iterator<String> iterator = lineStream.iterator();

			int founded = 0;
			while (iterator.hasNext()) {
				line = iterator.next();
				if (line == null || line.isEmpty()) {
					writer.newLine();
				} else {
					matcher = p.matcher(line);
					if (matcher.find()) {
						// System.out.println(matcher.group(2));
						// dimensionList.add(new
						// DimensionModel(matcher.group(1), matcher.group(2)));
						writer.write(line.replaceFirst(matcher.group(2), convertedList.get(founded).getValue()));
						writer.newLine();
						founded++;
					} else {
						writer.write(line);
						if (iterator.hasNext()) {
							writer.newLine();
						}
					}
				}
			}
			bufferedReader.close();
			writer.flush();
			writer.close();
			bufferedReader = null;
			writer = null;
		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}
				if (writer != null) {
					writer.flush();
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
