package io.file_chooser;

public interface FileSelectionListener {
	void onSelectedFile(String filePath);
}
