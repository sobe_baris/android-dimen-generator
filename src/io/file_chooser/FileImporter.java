package io.file_chooser;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class FileImporter {

	public static void showFileImporter(FileSelectionListener listener) {
		if (listener == null) {
			throw new NullPointerException("FileSelectionListener can not be null!");
		}
		
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("Android Dimension Generator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton button = new JButton("Import File");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					listener.onSelectedFile(fileChooser.getSelectedFile().getAbsolutePath());
					System.out.println(fileChooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		frame.add(button);
		frame.pack();
		Dimension size = frame.getContentPane().getSize();
		size.setSize(800, 200);
		frame.getContentPane().setSize(size);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
