package main;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pojo.DimensionModel;

public class DimenGenerator {

	private static Pattern sp = Pattern.compile("[0-9]+");

	public static List<DimensionModel> generateDefaultDimen(DimenType originalDimen, DimenType targetDimen,
			List<DimensionModel> originalDimensionList) {

		List<DimensionModel> convertedList = new ArrayList<>(originalDimensionList.size());

		double translateValue = DimenType.calculateTranslateValue(originalDimen, targetDimen);

		for (DimensionModel dimension : originalDimensionList) {
			StringBuilder value = new StringBuilder(dimension.getValue());
			double numericValue = 0;
			Matcher matcher = sp.matcher(value);

			if (matcher.find()) {
				numericValue = Integer.valueOf(matcher.group(0));
				int start = value.indexOf(String.valueOf((int) numericValue));
				int end = start + String.valueOf((int) numericValue).length();
				if (start == 0) {
					value.delete(start, end);
					value.insert(0, String.valueOf(getCalculatedValue(translateValue, numericValue)));
				}
			}
			convertedList.add(new DimensionModel(dimension.getKey(), value.toString()));
		}
		return convertedList;
	}

	private static int getCalculatedValue(double translateValue, double originalValue) {
		return (int) Math.round(originalValue * translateValue);
	}
}