package main;
public enum DimenType {
	D240, D320, D360, D480, Invalid;

	public static double calculateTranslateValue(DimenType originalDimen, DimenType targetDimen) {
		double value = 0;
		if (originalDimen == DimenType.D240) {
			if (targetDimen == DimenType.D240) {
				value = (double) 1;
			} else if (targetDimen == DimenType.D320) {
				value = (double) 4 / 3;
			} else if (targetDimen == DimenType.D360) {
				value = (double) 3 / 2;
			} else if (targetDimen == DimenType.D480) {
				value = (double) 2;
			}
		} else if (originalDimen == DimenType.D320) {
			if (targetDimen == DimenType.D240) {
				value = (double) 3 / 4;
			} else if (targetDimen == DimenType.D320) {
				value = (double) 1;
			} else if (targetDimen == DimenType.D360) {
				value = (double) 9 / 8;
			} else if (targetDimen == DimenType.D480) {
				value = (double) 3 / 2;
			}
		} else if (originalDimen == DimenType.D360) {
			if (targetDimen == DimenType.D240) {
				value = (double) 2 / 3;
			} else if (targetDimen == DimenType.D320) {
				value = (double) 8 / 9;
			} else if (targetDimen == DimenType.D360) {
				value = (double) 1;
			} else if (targetDimen == DimenType.D480) {
				value = (double) 4 / 3;
			}
		} else if (originalDimen == DimenType.D480) {
			if (targetDimen == DimenType.D240) {
				value = (double) 1 / 2;
			} else if (targetDimen == DimenType.D320) {
				value = (double) 2 / 3;
			} else if (targetDimen == DimenType.D360) {
				value = (double) 3 / 4;
			} else if (targetDimen == DimenType.D480) {
				value = (double) 1;
			}
		}
		return value;
	}

	public static DimenType valuesOf(String value) {
		if (value.contains("240")) {
			return DimenType.D240;
		} else if (value.contains("320")) {
			return DimenType.D320;
		} else if (value.contains("360")) {
			return DimenType.D360;
		} else if (value.contains("480")) {
			return DimenType.D480;
		}
		return DimenType.Invalid;
	}
}
