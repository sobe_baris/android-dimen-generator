package main;
import java.util.ArrayList;
import java.util.List;

public class DifferencesExporter {

	public static List<String> getDifferences(List<String> list1, List<String> list2) {
		List<String> target = new ArrayList<>(list1);
		target.removeAll(list2);
		return target;
	}
}
