package main;
import java.io.File;
import java.io.IOException;
import java.util.List;

import io.TextReader;
import io.TextWriter;
import io.folder_creator.FolderCreator;
import pojo.DimensionModel;

public class Main {

	/*
	private static final String dirRESOURCES = "resources";
	private static final String dirDIFFERENCES = "differences";
	private static final String fileName = "dimen.txt";
	*/
	
	public static void main(String[] args) throws IOException {
		
		if (args == null || args.length == 0 || args[0] == null) {
			System.err.println("Dimension of the reference values is missing!");
			System.err.println(Constants.EXAMPLE_USAGE);
			System.exit(-1);
		} else if (DimenType.valuesOf(args[0]) == DimenType.Invalid) {
			System.err.println("Supported dimension values ->\n240dp, 320dp, 360dpi 480dp");
			System.err.println(Constants.EXAMPLE_USAGE);
			System.exit(-1);
		} else if (args.length == 1 || args[1] == null) {
			System.err.println("Reference xml file is missing!");
			System.err.println(Constants.EXAMPLE_USAGE);
			System.exit(-1);
		} else if (!new File(args[1]).exists()) {
			System.err.println("Reference xml file can not found. Be sure input file is exists!");
			System.err.println(Constants.EXAMPLE_USAGE);
			System.exit(-1);
		} else if (args.length == 2 || args[2] == null) {
			System.err.println("Export path is missing!");
			System.err.println(Constants.EXAMPLE_USAGE);
			System.exit(-1);
		}

		DimenType referenceDimension = DimenType.valuesOf(args[0]);
		String referenceFilePath = args[1];
		String exportPath = args[2];

		TextReader textReaderTR = new TextReader(referenceFilePath);

		// ----------------------------------//
		List<DimensionModel> originalList = textReaderTR.getDimensionList();

		// ----------------------------------//
		new FolderCreator.Builder().setFolderName(exportPath).setDeleteExistingFolderIfExits(true).build();

		// ----------------------------------//
		List<DimensionModel> convertedList240 = DimenGenerator.generateDefaultDimen(referenceDimension, DimenType.D240,
				originalList);
		new FolderCreator.Builder().setDestinationPath(exportPath).setFolderName(Constants.EXPORT_PATH_240)
				.setDeleteExistingFolderIfExits(true).build();
		TextWriter.write(new File(referenceFilePath),
				new File(exportPath + Constants.EXPORT_PATH_240 + Constants.DIMENSION_FILE_NAME), convertedList240);

		// ----------------------------------//
		List<DimensionModel> convertedList320 = DimenGenerator.generateDefaultDimen(referenceDimension, DimenType.D320,
				originalList);
		new FolderCreator.Builder().setDestinationPath(exportPath).setFolderName(Constants.EXPORT_PATH_320)
				.setDeleteExistingFolderIfExits(true).build();
		TextWriter.write(new File(referenceFilePath),
				new File(exportPath + Constants.EXPORT_PATH_320 + Constants.DIMENSION_FILE_NAME), convertedList320);

		// ----------------------------------//
		List<DimensionModel> convertedList360 = DimenGenerator.generateDefaultDimen(referenceDimension, DimenType.D360,
				originalList);
		new FolderCreator.Builder().setDestinationPath(exportPath).setFolderName(Constants.EXPORT_PATH_360)
				.setDeleteExistingFolderIfExits(true).build();
		TextWriter.write(new File(referenceFilePath),
				new File(exportPath + Constants.EXPORT_PATH_360 + Constants.DIMENSION_FILE_NAME), convertedList360);

		// ----------------------------------//
		List<DimensionModel> convertedList480 = DimenGenerator.generateDefaultDimen(referenceDimension, DimenType.D480,
				originalList);
		new FolderCreator.Builder().setDestinationPath(exportPath).setFolderName(Constants.EXPORT_PATH_480)
				.setDeleteExistingFolderIfExits(true).build();
		TextWriter.write(new File(referenceFilePath),
				new File(exportPath + Constants.EXPORT_PATH_480 + Constants.DIMENSION_FILE_NAME), convertedList480);

		// ----------------------------------//
		System.out.println("Convertation is completed!");
		
		/*
		 * FileExporter.showFileExpoter(new FileSelectionListener() {
		 * 
		 * @Override public void onSelectedFile(String filePath) { // TODO
		 * Auto-generated method stub File file = new File(filePath);
		 * System.out.println(String.valueOf(file.exists())); } });
		 */
		/*
		 * for (int index = 0; index < originalList.size(); index++) {
		 * DimensionModel originalDimension = originalList.get(index);
		 * DimensionModel convertedDimension = convertedList.get(index);
		 * System.out.println(originalDimension.getKey() + " : " +
		 * originalDimension.getValue());
		 * System.out.println(convertedDimension.getKey() + " : " +
		 * convertedDimension.getValue()); }
		 */

		// TextWriter.write(dirRESOURCES, fileName, trList);
		// ----------------------------------//

		/*
		 * new
		 * FolderCreator.Builder().setDestinationPath(("/Users/baris/Desktop/"))
		 * .setFolderName("asd") .setDeleteExistingFolderIfExits(true).build();
		 */

		// TODO : FolderGenerator.class -> values, values-320dp, etc..				->	Completed
		// TODO : XMLGenerator.class -> dimen.xml for each folder					->	Not perfect but it works
		// TODO : UI -> Import - Export Button										->  ImportButton OK
		// TODO : FileImporter -> Import file from path								->  FileImporter OK
		// TODO : FileExporter -> Export folders to user specified path				->  FileExporter OK
		// TOOD : Multithread -> each folder process runs on different thread
	}
}
