package main;
public final class Constants {
	public static final String DIMENSION_FILE_NAME = "/dimens.xml";
	public static final String EXPORT_PATH_240 = "/values-sw240dp";
	public static final String EXPORT_PATH_320 = "/values-sw320dp";
	public static final String EXPORT_PATH_360 = "/values-sw360dp";
	public static final String EXPORT_PATH_480 = "/values-sw480dp";
	public static final String EXAMPLE_USAGE = "\nExample of the usage ->\njava -jar dimension-converter.jar 240dp input.xml exportPath";
}
